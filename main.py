import openpyxl
from openpyxl import Workbook
from openpyxl.styles import Alignment, Border, Side, PatternFill
from datetime import datetime

# Get current time
now = datetime.now()

# Format time as string
time_str = now.strftime('%y_%m_%d_%H_%M_%S')

# Create new workbook
wb = Workbook()


while True:
    ws = wb.create_sheet(input("The name of the scenario? "))
    host_speed = input("What will be the Host speeds? Km/h ")
    host_speed_list = [int(x) for x in host_speed.split()]
    target_speed = input("What will be the Target speeds? Km/h ")
    repetitions = int(input('How many repetitions for each speed? '))
    hit_point = int(input("What will be the hit point? "))
    header = ["Scenario", "Host speed Km/h", "Target speed Km/h", "Hit point", "Stopping distance (m)", "Driver action", "Results", "Specifications", "Comments", "Session Name"]
    ws.append(header)
    for row in ws.iter_rows(min_row=1, max_row=1):
        for cell in row:
            cell.alignment = Alignment(horizontal='center', vertical='center')
    line_number = 2
    for i in range(repetitions * len(host_speed_list)):
        ws[f'A{line_number}'] = ws.title
        line_number += 1
    line_number = 2
    for item in host_speed_list:
        for i in range(repetitions):
            ws[f'B{line_number}'] = item
            line_number += 1
    line_number = 2
    for i in range(repetitions * len(host_speed_list)):
        ws[f'C{line_number}'] = target_speed
        line_number += 1
    line_number = 2
    for i in range(repetitions * len(host_speed_list)):
        ws[f'D{line_number}'] = hit_point
        line_number += 1

    # Adjust width of columns to fit contents of cells
    for col in ws.columns:
        max_length = 0
        column = col[0].column_letter  # Get column letter
        for cell in col:
            try:
                if len(str(cell.value)) > max_length:
                    max_length = len(cell.value)
            except:
                pass
        adjusted_width = (max_length + 2) * 1.2  # Adjust width of column
        ws.column_dimensions[column].width = adjusted_width

    # Center align all cells and add borders around them
    for row in ws.iter_rows():
        for cell in row:
            cell.alignment = Alignment(horizontal='center')
            cell.border = Border(left=Side(style='thin'), right=Side(style='thin'), top=Side(style='thin'), bottom=Side(style='thin'))

    # Adding color to header row
    redFill = PatternFill(start_color='b3b3ff', end_color='b3b3ff', fill_type='solid')
    for row in ws['A1:J1']:
        for cell in row:
            cell.fill = redFill

    # Adding alternating fill colors to rows of each speed
    blueFill = PatternFill(start_color='ADD8E6', end_color='ADD8E6', fill_type='solid')
    yellowFill = PatternFill(start_color='FFFFE0', end_color='FFFFE0', fill_type='solid')
    current_fill = blueFill
    line_number = 2
    for item in host_speed_list:
        for i in range(repetitions):
            for row in ws[f'A{line_number}:J{line_number}']:
                for cell in row:
                    cell.fill = current_fill
            line_number += 1
        current_fill = yellowFill if current_fill == blueFill else blueFill

    if input('Do you want to create another sheet? (y/n) ').lower() != 'y':
        break

# Save file with formatted time string
filename = f'{time_str}.xlsx'
# Save the workbook
wb.save(filename)
